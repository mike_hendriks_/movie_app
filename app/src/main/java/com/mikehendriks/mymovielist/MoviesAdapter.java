package com.mikehendriks.mymovielist;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;


public class MoviesAdapter extends RecyclerView.Adapter<MoviesAdapter.MovieViewHolder> {
    public Context mCtx;
    private List<Movie> movieList;
    private RequestOptions options = new RequestOptions().placeholder(R.drawable.loading_shape).error(R.drawable.loading_shape);
//    private static final String TAG = "MoviesAdapter";

    public MoviesAdapter(Context mCtx, List<Movie> movieList) {
        this.mCtx = mCtx;
        this.movieList = movieList;
    }


    @Override
    public MovieViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view;
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        view = inflater.inflate(R.layout.movie_card, null);
        final MovieViewHolder viewHolder = new MovieViewHolder(view);
        viewHolder.movie_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(mCtx, MovieDetailsActivity.class);
                i.putExtra("movie_id", movieList.get(viewHolder.getAdapterPosition()).getId() );

                mCtx.startActivity(i);
            }
        });

        return viewHolder;
    }


    @Override
    public void onBindViewHolder(MovieViewHolder holder, int position) {
        Movie movie = movieList.get(position);

        Glide.with(mCtx)
                .load("https://image.tmdb.org/t/p/original/" + movie.getPoster_path())
                .apply(options)
                .into(holder.image_view);

        Log.e("ERROR", String.valueOf(movie.getId()));
        holder.et.setText(String.valueOf(movie.getId()));
        holder.title.setText(movie.getTitle());
        holder.rating.setText(String.valueOf(movie.getVote_average()));


//        holder.card.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Log.e(TAG, Integer.toString(R.id.movie_id));
////                FragmentManager manager = mCtx.getSupportFragmentManager();
////                manager.beginTransaction().replace(R.id.fragment_container, new MovieDetailsFragment()).commit();
////                Intent intent = new Intent(mCtx, MovieDetailsFragment.class);
////                intent.putExtra("EXTRA_SESSION_ID", R.id.movie_id);
////                mCtx.startActivity(intent);
//            }
//        });
    }

    @Override
    public int getItemCount() {
        return movieList.size();
    }


    public class MovieViewHolder extends RecyclerView.ViewHolder {

        EditText et;
        TextView title;
        TextView rating;
        ImageView image_view;
        CardView movie_card;

        public MovieViewHolder(View itemView) {
            super(itemView);

            mCtx = itemView.getContext();

            et =itemView.findViewById(R.id.movie_id);
            title =itemView.findViewById(R.id.title);
            rating =itemView.findViewById(R.id.rating);
            image_view =itemView.findViewById(R.id.image_view);
            movie_card = itemView.findViewById(R.id.Movie_card);
        }
    }

}
