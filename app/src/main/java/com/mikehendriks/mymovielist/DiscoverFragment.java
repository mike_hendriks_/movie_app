package com.mikehendriks.mymovielist;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class DiscoverFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_discover, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Set content for recyclerView1 - NOW PLAYING
        RecyclerView recyclerView1 = getView().findViewById(R.id.recycler_view1);
        recyclerView1.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        loadMovies("movie/now_playing", recyclerView1);

        // Set content for recyclerView2 - MOVIES TRENDING TODAY
        RecyclerView recyclerView2 = getView().findViewById(R.id.recycler_view2);
        recyclerView2.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        loadMovies("trending/movie/day", recyclerView2);

        // Set content for recyclerView3 - UPCOMING MOVIES
        RecyclerView recyclerView3 = getView().findViewById(R.id.recycler_view3);
        recyclerView3.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        loadMovies("movie/upcoming", recyclerView3);

        // Set content for recyclerView4 - TOP RATED - ALL TIME
        RecyclerView recyclerView4 = getView().findViewById(R.id.recycler_view4);
        recyclerView4.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        loadMovies("movie/top_rated", recyclerView4);
    }

    private void loadMovies(String request_url, final RecyclerView recyclerView) {

        // Build query string
        String api_url = "https://api.themoviedb.org/3/" + request_url + "?api_key=087956973e749023f4f56cd84dbc5427";

        // Initializing the movie list
        final List<Movie> movieList = new ArrayList<>();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, api_url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    // Converting the string to JSONArrayObject
                    JSONArray array = jsonObject.getJSONArray("results");

                    // Traversing through all the objects
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject movie = array.getJSONObject(i);

                        movieList.add(new Movie(
                                movie.getInt("id"),
                                movie.getString("title"),
                                movie.getString("poster_path"),
                                movie.getDouble("vote_average")
                        ));
                    }

                    MoviesAdapter adapter = new MoviesAdapter(getActivity(), movieList);
                    recyclerView.setAdapter(adapter);


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        },
        new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        Volley.newRequestQueue(getContext()).add(stringRequest);

    }
}
