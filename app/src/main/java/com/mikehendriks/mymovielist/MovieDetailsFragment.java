package com.mikehendriks.mymovielist;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

public class MovieDetailsFragment extends Fragment {

    private static final String ARG_ID = " ";
    public String MovieId;

//    String sessionId= getActivity().getIntent().getStringExtra("EXTRA_SESSION_ID");

    public static MovieDetailsFragment newInstance(String ID) {
        MovieDetailsFragment fragment = new MovieDetailsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_ID, ID);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.movie_details, container, false);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (getArguments() !=null){
            MovieId = getArguments().getString(ARG_ID);
        }

        TextView movieName = getView().findViewById(R.id.Movie_Name);
        movieName.setText(MovieId);

        loadMovie("movie/" + MovieId);

    }

    private void loadMovie(String request_url) {

        String api_url = "https://api.themoviedb.org/3/" + request_url + "?api_key=087956973e749023f4f56cd84dbc5427";

        StringRequest stringRequest = new StringRequest(Request.Method.GET, api_url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                }finally {

                }
                }

        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });

        Volley.newRequestQueue(getContext()).add(stringRequest);


    }
}
