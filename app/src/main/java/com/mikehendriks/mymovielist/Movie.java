package com.mikehendriks.mymovielist;

public class Movie {
    private int id;
    private String title;
    private String poster_path;
    private Number vote_average;

    public Movie(){
    };

    public Movie (int id, String title, String poster_path, Number vote_average) {
        this.id = id;
        this.title = title;
        this.poster_path = poster_path;
        this.vote_average = vote_average;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getPoster_path() {
        return poster_path;
    }

    public Number getVote_average() {
        return vote_average;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setPoster_path(String poster_path) {
        this.poster_path = poster_path;
    }

    public void setVote_average(Number vote_average) {
        this.vote_average = vote_average;
    }


}
