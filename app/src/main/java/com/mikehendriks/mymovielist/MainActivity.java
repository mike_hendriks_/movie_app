package com.mikehendriks.mymovielist;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

public class MainActivity extends AppCompatActivity {

    EditText mEdit;

    private static final int ERROR_DIALOG_REQUEST = 9001;
    private static final String TAG = "MainActivity";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getSupportActionBar().setElevation(0);
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_main );

        BottomNavigationView bottomNav = findViewById( R.id.bottom_navigation );
        bottomNav.setOnNavigationItemSelectedListener(navListener);

        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                new WatchlistFragment()).commit();


    }

    private BottomNavigationView.OnNavigationItemSelectedListener navListener =
        new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

                Fragment selectedFragment = null;

                switch (menuItem.getItemId()) {
                    case R.id.navigation_watchlist_activity:
                        selectedFragment = new WatchlistFragment();
                        break;

                    case R.id.navigation_cinema_activity:
                        selectedFragment = new CinemaFragment();
                        break;

                    case R.id.navigation_discover_activity:
                        selectedFragment = new DiscoverFragment();
                        break;
                }

                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        selectedFragment).commit();

                return true;
            }
        };

//    public void GoToMovie(View view) {
//
//                mEdit = findViewById(R.id.movie_id);
//                Log.e("ERROR", mEdit.getText().toString());
//
//                MovieDetailsFragment fragment = MovieDetailsFragment.newInstance(mEdit.getText().toString());
//
//                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,fragment).commit();
//
//
//
//
////                Intent intent = new Intent(this
////                , MovieDetailsFragment.class);
////                intent.putExtra("EXTRA_SESSION_ID", R.id.movie_id);
////                this.startActivity(intent);
//
//    }

    private void init() {

    }


    public boolean isServicesOk() {
        Log.d(TAG, "is_services_ok: Checking google play services version");
        int available = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(MainActivity.this);

        if (available == ConnectionResult.SUCCESS){
//            Everything is fine and the user can make map requests
            Log.d(TAG, "isServicesOk: Google play services is working");
            return true;
        } else if (GoogleApiAvailability.getInstance().isUserResolvableError(available)){
//            An error occured but we can resolve it
            Log.d(TAG, "isServicesOk: An error occured but we can resolve it");
            Dialog dialog = GoogleApiAvailability.getInstance().getErrorDialog(MainActivity.this, available, ERROR_DIALOG_REQUEST);
            dialog.show();
        } else {
            Toast.makeText(this, "You can't make map request", Toast.LENGTH_SHORT).show();
        }
        return false;
    }

}
