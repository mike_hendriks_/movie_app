package com.mikehendriks.mymovielist;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class MovieDetailsActivity extends AppCompatActivity {


    private String trailer_key;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.movie_details);

        final ImageView image_background = findViewById(R.id.MovieTopDetails);
        final TextView title_details = findViewById(R.id.Movie_Name);
        final TextView rating = findViewById(R.id.Ratings);
        final TextView plot_text = findViewById(R.id.plot_text);
        final TextView release_date = findViewById(R.id.release_date);
        final TextView status = findViewById(R.id.status);
        final TextView runtime = findViewById(R.id.runtime);
        final TextView budget = findViewById(R.id.budget);
        final TextView revenue = findViewById(R.id.revenue);
        final TextView cast = findViewById(R.id.cast);
         final ArrayList<String> list = new ArrayList<>();


        int request_id = getIntent().getExtras().getInt("movie_id");


        String api_url_movie = "https://api.themoviedb.org/3/movie/" + String.valueOf(request_id) + "?api_key=087956973e749023f4f56cd84dbc5427";
        String api_url_video = "https://api.themoviedb.org/3/movie/" + String.valueOf(request_id) + "/videos?api_key=087956973e749023f4f56cd84dbc5427";
        String api_url_cast = "https://api.themoviedb.org/3/movie/" + String.valueOf(request_id) + "/credits?api_key=087956973e749023f4f56cd84dbc5427";


        JsonObjectRequest jsonObjectRequestMovie = new JsonObjectRequest(Request.Method.GET, api_url_movie,null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response_movie) {
                try {
                    Log.e("ERROR", String.valueOf(response_movie.getString("original_title")));
                    Glide.with(MovieDetailsActivity.this).load("https://image.tmdb.org/t/p/w780/" + (String.valueOf(response_movie.getString("backdrop_path")))).into(image_background);
                    title_details.setText(String.valueOf(response_movie.getString("original_title")));
                    rating.setText("\u2605 " + String.valueOf(response_movie.getString("vote_average")) + " | " + String.valueOf(response_movie.getString("vote_count")) + " Votes");
                    plot_text.setText(String.valueOf(response_movie.getString("overview")));

//                  changing release date format to correct format
                    String date = response_movie.getString("release_date");
                    SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd");
                    SimpleDateFormat output = new SimpleDateFormat("dd-MM-yyyy");

                    try {
                        Date date_input = input.parse(date);                 // parse input
                        release_date.setText(output.format(date_input));    // format output
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    status.setText(response_movie.getString("status"));
                    runtime.setText(response_movie.getString("runtime") + " Minutes");
                    budget.setText("$" + String.valueOf(response_movie.getString("budget")));
                    revenue.setText("$" + String.valueOf(response_movie.getString("revenue")));

                    //TODO: Add other things to details page

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        },new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                // TODO: Handle error

            }
        });

        Volley.newRequestQueue(this).add(jsonObjectRequestMovie);

        JsonObjectRequest jsonObjectRequestVideo = new JsonObjectRequest(Request.Method.GET, api_url_video,null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response_video) {
                try {
                    JSONArray array = response_video.getJSONArray("results");

                    for (int i = 0; i < array.length(); i++) {
                        JSONObject trailer = array.getJSONObject(0);

                         trailer_key = trailer.getString("key");
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        },new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                // TODO: Handle error

            }
        });

        Volley.newRequestQueue(this).add(jsonObjectRequestVideo);

        JsonObjectRequest jsonObjectRequestCast = new JsonObjectRequest(Request.Method.GET, api_url_cast,null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response_cast) {
                StringBuilder sb = new StringBuilder();
                try {
                    JSONArray array = response_cast.getJSONArray("cast");

                    for (int e = 0; e < array.length(); e++) {
                        JSONObject cast = array.getJSONObject(e);

                        list.add(cast.getString("name"));


                    }

                    String prefix = "";
                    for (String str : list) {
                        sb.append(prefix);
                        prefix = ", ";
                        sb.append(str);
                    }
                    String Cast_list = sb.toString();
                    cast.setText(Cast_list);


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        },new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                // TODO: Handle error

            }
        });

        Volley.newRequestQueue(this).add(jsonObjectRequestCast);





    }


    public void Back(View view) {
        finish();
    }

    public void GoToTrailer(View view) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/watch?v=" + trailer_key));
        startActivity(browserIntent);
    }

    public void AddToMovielist(View view) {
        FloatingActionButton watchlist_btn = findViewById(R.id.watchlist_btn);
        if (view == watchlist_btn){
            watchlist_btn.setImageResource(R.drawable.ic_baseline_playlist_add_check_24px);
        }
    }
}
